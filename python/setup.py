import os
import platform
from distutils.core import setup, Extension
from distutils.command.build_ext import build_ext

include_dirs = [
    '../server/',
    '../third_party/libjson-rpc-cpp/src/',
    '../third_party/indri/include/',
    '../third_party/indri/contrib/lemur/include/',
]

libraries = [
    # 'stdc++',
    # 'sifaka',
    'jsonrpc',
    'indri',
    'lemur',
    'antlr',
    'xpdf',
]

extra_compile_args = [
    '-Wno-reorder',
    '-Wno-unused-variable',
    '-DP_NEEDS_GNU_CXX_NAMESPACE=1',
    # '-DHAVE_EXT_ATOMICITY_H',
    # '-arch i386',
    # '-m32',
]

extra_link_args = [
    '-lz',
]

if platform.system() == "Darwin":
    extra_compile_args += [
        '-Wno-deprecated-writable-strings',
        '-stdlib=libstdc++',
    ]
    extra_link_args += [
        '-stdlib=libstdc++',
    ]

ext_modules = []

if os.path.exists('../out/Debug'):
    ext_modules += [
        Extension('_sifaka_debug',
                  sources=['_sifaka.cpp'],
                  include_dirs=include_dirs,
                  libraries=['sifaka'] + libraries,
                  library_dirs=['../out/Debug'],
                  extra_compile_args=extra_compile_args,
                  extra_link_args=extra_link_args),
        Extension('_sifaka_test',
                  sources=['_sifaka.cpp', 'mock_sifaka.cpp'],
                  include_dirs=include_dirs,
                  libraries=libraries,
                  library_dirs=['../out/Debug'],
                  extra_compile_args=extra_compile_args,
                  extra_link_args=extra_link_args),
    ]

if os.path.exists('../out/Release'):
    ext_modules += [
        Extension('_sifaka_release',
                  sources=['_sifaka.cpp'],
                  include_dirs=include_dirs,
                  libraries=['sifaka'] + libraries,
                  library_dirs=['../out/Release'],
                  extra_compile_args=extra_compile_args,
                  extra_link_args=extra_link_args),
    ]

setup(name='pysifaka',
      ext_modules=ext_modules)
