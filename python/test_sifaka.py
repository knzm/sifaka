# -*- coding: utf-8 -*-

import unittest
import sys
import json

ARGUMENT_SIZE_ERROR_MSG = "function takes at most {0} arguments ({1} given)"
NO_ARGUMENT_ERROR_MSG = "Required argument '{0}' (pos {1}) not found"
TYPE_ERROR_MSG = 'must be {0}, not {1}'

if sys.version_info >= (2, 7, 0):
    from unittest.case import _AssertRaisesContext

else:
    class _AssertRaisesContext(object):
        """A context manager used to implement TestCase.assertRaises* methods."""

        def __init__(self, expected, test_case):
            self.expected = expected
            self.failureException = test_case.failureException

        def __enter__(self):
            return self

        def __exit__(self, exc_type, exc_value, tb):
            if exc_type is None:
                try:
                    exc_name = self.expected.__name__
                except AttributeError:
                    exc_name = str(self.expected)
                raise self.failureException(
                    "{0} not raised".format(exc_name))
            if not issubclass(exc_type, self.expected):
                # let unexpected exceptions pass through
                return False

            return True


class SifakaTest(unittest.TestCase):
    def get_MUT(self):
        import _sifaka
        return _sifaka

    def assert_json_equals(self, json_string, expected_value):
        self.assertEquals(json.loads(json_string), expected_value)


class TestRunQuery(SifakaTest):
    def get_FUT(self):
        return self.get_MUT().run_query

    def test_empty_params(self):
        target = self.get_FUT()
        ret = target("query", {})
        self.assert_json_equals(ret, {})

    def test_unicode_key(self):
        target = self.get_FUT()
        ret = target("query", {u"unicode":0})
        self.assert_json_equals(ret, {})

    def test_key_is_not_str(self):
        target = self.get_FUT()
        with _AssertRaisesContext(TypeError, self):
            try:
                target("query", {1:2})
            except Exception, exc:
                raise
        expected_message = "key must be string"
        self.assertEquals(exc.args, (expected_message,))

    def test_value_is_none(self):
        target = self.get_FUT()
        with _AssertRaisesContext(TypeError, self):
            try:
                target("query", {"key": None})
            except Exception, exc:
                raise
        expected_message = "value must either str, bool, int or float"
        self.assertEquals(exc.args, (expected_message,))

    def test_bool_value(self):
        target = self.get_FUT()
        ret = target("query", {"key": True})
        self.assert_json_equals(ret, {})

    def test_int_value(self):
        target = self.get_FUT()
        ret = target("query", {"key": 0})
        self.assert_json_equals(ret, {})

    def test_float_value(self):
        target = self.get_FUT()
        ret = target("query", {"key": 0.0})
        self.assert_json_equals(ret, {})

    def test_str_value(self):
        target = self.get_FUT()
        ret = target("query", {"key": "val"})
        self.assert_json_equals(ret, {})

    def test_unicode_value(self):
        target = self.get_FUT()
        ret = target("query", {"key": u"val"})
        self.assert_json_equals(ret, {})

    def test_no_arguments(self):
        target = self.get_FUT()
        with _AssertRaisesContext(TypeError, self):
            try:
                target()
            except Exception, exc:
                raise
        expected_message = NO_ARGUMENT_ERROR_MSG.format('query', 1)
        self.assertEquals(exc.args, (expected_message,))

    def test_one_arguments(self):
        target = self.get_FUT()
        with _AssertRaisesContext(TypeError, self):
            try:
                target("query")
            except Exception, exc:
                raise
        expected_message = NO_ARGUMENT_ERROR_MSG.format('query_params', 2)
        self.assertEquals(exc.args, (expected_message,))

    def test_extra_positional_arguments(self):
        target = self.get_FUT()
        with _AssertRaisesContext(TypeError, self):
            try:
                target("query", {}, None)
            except Exception, exc:
                raise
        expected_message = ARGUMENT_SIZE_ERROR_MSG.format(2, 3)
        self.assertEquals(exc.args, (expected_message,))

    def test_extra_keyword_arguments(self):
        target = self.get_FUT()
        with _AssertRaisesContext(TypeError, self):
            try:
                target("query", {}, extra=True)
            except Exception, exc:
                raise
        expected_message = ARGUMENT_SIZE_ERROR_MSG.format(2, 3)
        self.assertEquals(exc.args, (expected_message,))

    def test_keyword_arguments(self):
        target = self.get_FUT()
        ret = target(query="query", query_params={})
        self.assert_json_equals(ret, {})

    def test_arg1_is_none(self):
        target = self.get_FUT()
        with _AssertRaisesContext(TypeError, self):
            try:
                target(None, {})
            except Exception, exc:
                raise
        expected_message = "argument 1 must be string, not None"
        self.assertEquals(exc.args, (expected_message,))

    def test_arg1_is_not_str(self):
        target = self.get_FUT()
        with _AssertRaisesContext(TypeError, self):
            try:
                target(0, {})
            except Exception, exc:
                raise
        expected_message = "argument 1 must be string, not int"
        self.assertEquals(exc.args, (expected_message,))

    def test_arg1_is_unicode(self):
        target = self.get_FUT()
        target(u"u", {})

    def test_arg2_is_none(self):
        target = self.get_FUT()
        with _AssertRaisesContext(TypeError, self):
            try:
                target("query", None)
            except Exception, exc:
                raise
        expected_message = "must be mapping"
        self.assertEquals(exc.args, (expected_message,))

    def test_arg2_is_not_mapping(self):
        target = self.get_FUT()
        with _AssertRaisesContext(TypeError, self):
            try:
                target("query", [])
            except Exception, exc:
                raise
        expected_message = "must be mapping"
        self.assertEquals(exc.args, (expected_message,))


class TestDumpIndex(SifakaTest):
    def get_FUT(self):
        return self.get_MUT().dumpindex

    def test_command_no_args(self):
        target = self.get_FUT()
        ret = target("index", "command", [])
        self.assert_json_equals(ret, {})

    def test_no_args(self):
        target = self.get_FUT()
        with _AssertRaisesContext(TypeError, self):
            try:
                target()
            except Exception, exc:
                raise
        expected_message = NO_ARGUMENT_ERROR_MSG.format('index', 1)
        self.assertEquals(exc.args, (expected_message,))


class TestBuildIndex(SifakaTest):
    def get_FUT(self):
        return self.get_MUT().build_index

    def test_fast(self):
        target = self.get_FUT()
        ret = target({}, "class", [])
        self.assertFalse(ret)


if __name__ == '__main__':
    unittest.main()
