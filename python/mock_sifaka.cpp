#include <string>
#include <vector>
#include <jsonrpc/rpc.h>

#include "indri/Parameters.hpp"
#include "indri/IndexEnvironment.hpp"

#include "RunQuery.h"
#include "dumpindex.h"
#include "BuildIndex.h"


void IndriRunQueryMain(Json::Value& response, indri::api::Parameters& param)
{
    response = Json::Value(Json::objectValue);
}

Json::Value dumpindex_main(const std::string repName, const std::string command, const std::vector< std::string > args)
{
    Json::Value value(Json::objectValue);
    return value;
}

bool BuildIndexMain(/* const */ indri::api::Parameters& parameters,
                    const std::string& fileClass,
                    const std::vector< document_t >& documents)
{
    return false;
}
