#include <iostream>

#include <Python.h>
#include <jsonrpc/rpc.h>

#include "indri/Parameters.hpp"
#include "indri/IndexEnvironment.hpp"

#include "RunQuery.h"
#include "dumpindex.h"
#include "BuildIndex.h"

static PyObject *SifakaError;

/*
  Set indri::api::Parameters with key and value
 */
static bool parameters_set(indri::api::Parameters& indri_param, PyObject* key, PyObject* value)
{
    key = PyUnicode_FromObject(key);
    if (key == NULL) {
        PyErr_SetString(PyExc_TypeError, "key must be string");
        return false;
    }

    PyObject* key_str = PyUnicode_AsUTF8String(key);
    if (key_str == NULL) {
        Py_DECREF(key);
        return false;
    }
    const char* key_cstr = PyString_AS_STRING(key_str);

    bool ret = false;

    if (PyBool_Check(value)) {
        bool bool_val = (bool)(((PyBoolObject *)value)->ob_ival);
        indri_param.set(key_cstr, bool_val);
        ret = true;
    }
    else if (PyInt_Check(value)) {
        long long_val = PyInt_AsLong(value);
        indri_param.set(key_cstr, (int)long_val);
        ret = true;
    }
    else if (PyFloat_Check(value)) {
        double double_val = PyFloat_AsDouble(value);
        if (PyErr_Occurred())
            goto error;
        indri_param.set(key_cstr, double_val);
        ret = true;
    }
    else if (PyUnicode_Check(value) || PyString_Check(value)) {
        value = PyUnicode_FromObject(value);
        if (value == NULL)
            goto error;
        PyObject* value_str = PyUnicode_AsUTF8String(value);
        if (value_str == NULL) {
            Py_DECREF(value);
            goto error;
        }
        const char* value_cstr = PyString_AS_STRING(value_str);
        indri_param.set(key_cstr, value_cstr);
        Py_DECREF(value_str);
        Py_DECREF(value);
        ret = true;
    }
    else {
        PyErr_SetString(PyExc_TypeError, "value must either str, bool, int or float");
        ret = false;
    }

  error:
    Py_XDECREF(key_str);
    Py_XDECREF(key);

    return ret;
}

/*
  Set std::vector< std::string > with PyStringObject/PyUnicodeObject
 */
static bool string_vector_set(std::vector< std::string >& args, PyObject* py_args)
{
    Py_ssize_t size = PySequence_Size(py_args);
    if (size < 0)
        return false;

    for (Py_ssize_t i = 0; i < size; i++) {
        PyObject* arg = PySequence_GetItem(py_args, i);
        arg = PyUnicode_FromObject(arg);
        if (arg == NULL)
            return false;
        PyObject* arg_str = PyUnicode_AsUTF8String(arg);
        if (arg_str == NULL) {
            Py_DECREF(arg);
            return false;
        }
        args.push_back(PyString_AS_STRING(arg_str));
        Py_DECREF(arg_str);
        Py_DECREF(arg);
    }
    return true;
}

static PyObject* PyMapping_iteritems(PyObject* o)
{
    PyObject* items = PyMapping_Items(o);
    if (items == NULL)
        return NULL;

    PyObject* it = PyObject_GetIter(items);
    Py_DECREF(items);
    return it;
}

static bool parameters_set_by_mapping(indri::api::Parameters& params, PyObject* obj)
{
    if (!PyMapping_Check(obj)) {
        PyErr_SetString(PyExc_TypeError, "must be mapping");
        return false;
    }

    PyObject* it = PyMapping_iteritems(obj);
    if (it == NULL)
        return false;

    PyObject* item;
    while ((item = PyIter_Next(it)) != NULL) {
        if (!PyTuple_Check(item) || Py_SIZE(item) != 2) {
            PyErr_SetString(PyExc_ValueError, "items must return 2-tuples");
            Py_DECREF(it);
            return false;
        }
        PyObject* key = PyTuple_GET_ITEM(item, 0);
        PyObject* value = PyTuple_GET_ITEM(item, 1);
        if (!parameters_set(params, key, value)) {
            Py_DECREF(it);
            return false;
        }
    }
    Py_DECREF(it);

    return true;
}

/*
  run_query(query, query_params)
*/
static PyObject* run_query(PyObject* self, PyObject* args, PyObject* kwd)
{
    PyObject* query;
    PyObject* query_params;

    static char* kw_list[] = {"query", "query_params", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kwd, "OO", kw_list,
                                     &query, &query_params))
        return NULL;

    if (! (PyUnicode_Check(query) || PyString_Check(query))) {
        assert(query != NULL);
        std::stringstream s;
        s << "argument 1 must be string, not "
          << (query == Py_None ? "None" : query->ob_type->tp_name);
        PyErr_SetString(PyExc_TypeError, s.str().c_str());
        return NULL;
    }

    indri::api::Parameters indri_param;
    {
      PyObject* key = PyString_FromString("query");
      if (key == NULL)
          return NULL;
      if (!parameters_set(indri_param, key, query)) {
          Py_DECREF(key);
          return NULL;
      }
      Py_DECREF(key);
    }

    if (!parameters_set_by_mapping(indri_param, query_params))
        return NULL;

    Json::Value response;
    try {
        IndriRunQueryMain(response, indri_param);
    } catch( lemur::api::Exception& e ) {
        std::stringstream s;
        e.writeMessage(s);
        PyErr_SetString(SifakaError, s.str().c_str());
        return NULL;
    }

    Json::FastWriter w;
    std::string response_str = w.write(response);

    return PyUnicode_FromString(response_str.c_str());
}

/*
  dumpindex(index, command, command_args)
*/
static PyObject* dumpindex(PyObject* self, PyObject* args, PyObject* kwd)
{
    char* index;
    char* command;
    PyObject* py_command_args;

    static char* kw_list[] = {"index", "command", "command_args", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kwd, "ssO", kw_list,
                                     &index, &command, &py_command_args))
        return NULL;

    std::vector< std::string > command_args;

    if (!string_vector_set(command_args, py_command_args))
        return NULL;

    const Json::Value& response = dumpindex_main(index, command, command_args);

    Json::FastWriter w;
    std::string response_str = w.write(response);

    return PyUnicode_FromString(response_str.c_str());
}

/*
  build_index(build_params, file_class, document_list)
*/
static PyObject* build_index(PyObject* self, PyObject* args, PyObject* kwd)
{
    PyObject* build_params;
    char* file_class;
    PyObject* document_list;

    static char* kw_list[] = {"build_params", "file_class", "document_list", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, kwd, "OsO", kw_list,
                                     &build_params, &file_class, &document_list))
        return NULL;

    indri::api::Parameters indri_param;
    if (!parameters_set_by_mapping(indri_param, build_params))
        return NULL;

    std::vector< document_t > documents;

    Py_ssize_t size = PySequence_Size(document_list);
    if (size < 0)
        return NULL;

    for (Py_ssize_t i = 0; i < size; i++) {
        PyObject* doc = PySequence_GetItem(document_list, i);
        if (!PyMapping_Check(doc))
            return NULL;

        PyObject* document = PyMapping_GetItemString(doc, "document");
        if (document == NULL)
            return NULL;
        document = PyUnicode_FromObject(document);
        if (document == NULL)
            return NULL;
        PyObject* document_str = PyUnicode_AsUTF8String(document);
        if (document_str == NULL) {
            Py_DECREF(document);
            return NULL;
        }
        std::string documentString = PyString_AS_STRING(document_str);
        Py_DECREF(document_str);
        Py_DECREF(document);

        PyObject* metadata_obj = PyMapping_GetItemString(doc, "metadata");
        if (metadata_obj == NULL)
            return NULL;

        metadata_t metadata;

        PyObject* it = PyMapping_iteritems(metadata_obj);
        if (it == NULL)
            return NULL;

        PyObject* item;
        while ((item = PyIter_Next(it)) != NULL) {
            if (!PyTuple_Check(item) || Py_SIZE(item) != 2) {
                PyErr_SetString(PyExc_ValueError, "items must return 2-tuples");
                Py_DECREF(it);
                return NULL;
            }
            PyObject* key = PyTuple_GET_ITEM(item, 0);
            PyObject* value = PyTuple_GET_ITEM(item, 1);
            key = PyUnicode_FromObject(key);
            value = PyUnicode_FromObject(value);
            if (key == NULL || value == NULL) {
                Py_XDECREF(key);
                Py_XDECREF(value);
                Py_DECREF(it);
                return NULL;
            }

            PyObject* key_str = PyUnicode_AsUTF8String(key);
            PyObject* value_str = PyUnicode_AsUTF8String(value);
            if (key_str == NULL || value_str == NULL) {
                Py_XDECREF(key_str);
                Py_XDECREF(value_str);
                Py_DECREF(key);
                Py_DECREF(value);
                Py_DECREF(it);
            }

            const char* key_cstr = PyString_AS_STRING(key_str);
            const char* value_cstr = PyString_AS_STRING(value_str);

            std::pair< std::string, std::string > pair;
            pair.first = key_cstr;
            pair.second = value_cstr;

            metadata.push_back(pair);

            Py_DECREF(key_cstr);
            Py_DECREF(value_cstr);
            Py_DECREF(key);
            Py_DECREF(value);
        }
        Py_DECREF(it);

        documents.push_back(document_t(documentString, metadata));
    }

    bool ret;
    try {
        ret = BuildIndexMain(indri_param, file_class, documents);
    } catch( lemur::api::Exception& e ) {
        std::stringstream s;
        e.writeMessage(s);
        PyErr_SetString(SifakaError, s.str().c_str());
        return NULL;
    }

    return PyBool_FromLong(ret);
}

static PyMethodDef Sifaka_methods[] = {
    {"run_query",   (PyCFunction)run_query,   METH_VARARGS | METH_KEYWORDS,
     NULL},
    {"dumpindex",   (PyCFunction)dumpindex,   METH_VARARGS | METH_KEYWORDS,
     NULL},
    {"build_index", (PyCFunction)build_index, METH_VARARGS | METH_KEYWORDS,
     NULL},
    {NULL, NULL}
};

PyMODINIT_FUNC
init_sifaka(void)
{
    PyObject *mod = Py_InitModule("_sifaka", Sifaka_methods);

    SifakaError = PyErr_NewException("_sifaka.SifakaError",
                                     PyExc_StandardError, NULL);
    if (SifakaError == NULL)
        goto error;

    Py_INCREF(SifakaError);
    if (PyModule_AddObject(mod, "SifakaError", SifakaError) < 0)
        goto error;

error:
    if (PyErr_Occurred())
    {
        PyErr_SetString(PyExc_ImportError, "_sifaka: init failed");
    }
}
