#!/bin/sh -e

python=${PYTHON:-python}

rm -rf build _sifaka.so

platlib=$($python -c 'import sys, distutils.util; print "lib.%s-%d.%d" % ((distutils.util.get_platform(),) + sys.version_info[:2])')
$python setup.py build

if [ -f build/${platlib}/_sifaka_test.so ]; then
  ln -s build/${platlib}/_sifaka_test.so _sifaka.so
  $python test_sifaka.py
  rm _sifaka.so
fi

if [ -f build/${platlib}/_sifaka_release.so ]; then
  ln -s build/${platlib}/_sifaka_release.so _sifaka.so
elif [ -f build/${platlib}/_sifaka_debug.so ]; then
  ln -s build/${platlib}/_sifaka_debug.so _sifaka.so
fi
$python -c 'import _sifaka'
