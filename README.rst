====================
Sifaka Indri Server
====================

Overview
----------

JSON-RPC Server on Indri

Get the Code
--------------------

First, you'll need to download the `depot_tools <http://dev.chromium.org/developers/how-tos/install-depot-tools>`_, and add depot_tools to your PATH. 

::

  $ cd ~/src
  $ svn co http://src.chromium.org/svn/trunk/tools/depot_tools
  $ export PATH="$PATH":`pwd`/depot_tools

Then, download the code from its git repository with the gclient command:

::

  $ mkdir -p ~/src/sifaka
  $ cd ~/src/sifaka
  $ gclient config git+https://bitbucket.org/knzm/sifaka
  $ gclient sync

This will also download source code of other programs such as libjson-rpc-cpp and indri. 

Compilation
--------------------

::

  $ cd ~/src/sifaka/sifaka
  $ python build_sifaka.py gyp
  $ python build_sifaka.py build

If you want a release build, then::

  $ python build_sifaka.py build -c Release

Or you can get a 64-bit build by::

  $ python build_sifaka.py build -c Release -a x64

.. note::

   On FreeBSD, you should use `gmake` command instead of `make`.
   Moreover, you may have to specify a compiler name such as `g++47`.
   So the command line looks like:

   ::

     $ BUILD_COMMAND=gmake CXX=g++47 python build_sifaka.py build
