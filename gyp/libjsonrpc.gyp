{
  'includes': [
    'common.gypi',
  ],
  'targets': [
    {
      'target_name': 'libjsonrpc',
      'type': 'static_library',
      'standalone_static_library': 1,
      'variables': {
        'libjsonrpc_root': '<(third_party_dir)/libjson-rpc-cpp',
        'jsoncpp_root': '<(libjsonrpc_root)/src/jsonrpc/json',
      },
      'defines': [
        'DEBUG_TRACE',
      ],
      'sources': [
        '<(libjsonrpc_root)/src/jsonrpc/abstractauthenticator.h',
        '<(libjsonrpc_root)/src/jsonrpc/abstractrequesthandler.h',
        '<(libjsonrpc_root)/src/jsonrpc/client.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/client.h',
        '<(libjsonrpc_root)/src/jsonrpc/clientconnector.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/clientconnector.h',
        '<(libjsonrpc_root)/src/jsonrpc/errors.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/errors.h',
        '<(libjsonrpc_root)/src/jsonrpc/exception.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/exception.h',
        '<(libjsonrpc_root)/src/jsonrpc/procedure.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/procedure.h',
        '<(libjsonrpc_root)/src/jsonrpc/rpc.h',
        '<(libjsonrpc_root)/src/jsonrpc/rpcprotocolclient.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/rpcprotocolclient.h',
        '<(libjsonrpc_root)/src/jsonrpc/rpcprotocolserver.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/rpcprotocolserver.h',
        '<(libjsonrpc_root)/src/jsonrpc/server.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/server.h',
        '<(libjsonrpc_root)/src/jsonrpc/serverconnector.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/serverconnector.h',
        '<(libjsonrpc_root)/src/jsonrpc/specification.h',
        '<(libjsonrpc_root)/src/jsonrpc/specificationparser.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/specificationparser.h',
        '<(libjsonrpc_root)/src/jsonrpc/specificationwriter.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/specificationwriter.h',
        '<(libjsonrpc_root)/src/jsonrpc/json/json_reader.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/json/json_value.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/json/json_writer.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/connectors/httpclient.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/connectors/httpserver.cpp',
        '<(libjsonrpc_root)/src/jsonrpc/connectors/mongoose.c',
      ],
      'direct_dependent_settings': {
        'include_dirs': [
          '<(libjsonrpc_root)/src/jsonrpc',
          '<(libjsonrpc_root)/src',
        ],
      },
      'conditions': [
        ['OS=="linux"', {
          'link_settings': {
            'libraries+': [
              '-ldl',
            ],
          },
        }],
      ],
    },
  ],
}
