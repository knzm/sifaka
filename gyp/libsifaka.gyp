{
  'includes': [
    'common.gypi',
  ],
  'variables': {
    'server_root': '<(DEPTH)/server',
    'indri_root': '<(third_party_dir)/indri',
    'lemur_root': '<(third_party_dir)/indri/contrib/lemur',
    'antlr_root': '<(third_party_dir)/indri/contrib/antlr',
  },
  'targets': [
    {
      'target_name': 'libsifaka',
      'type': 'static_library',
      'standalone_static_library': 1,
      'sources': [
        '<(server_root)/RunQuery.cpp',
        '<(server_root)/dumpindex.cpp',
        '<(server_root)/BuildIndex.cpp',
      ],
      'include_dirs': [
        '<(indri_root)/include',
        '<(indri_root)/include/indri',
        '<(lemur_root)/include',
        '<(lemur_root)/include/lemur',
        '<(antlr_root)/include',
        '<(antlr_root)/include/antlr',
      ],
      'dependencies': [
        'libjsonrpc.gyp:libjsonrpc',
        'indri.gyp:libindri',
        'indri.gyp:liblemur',
      ],
    },
  ],
}
