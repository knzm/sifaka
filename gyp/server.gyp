{
  'includes': [
    'common.gypi',
  ],
  'variables': {
    'server_root': '<(DEPTH)/server',
    'indri_root': '<(third_party_dir)/indri',
    'lemur_root': '<(third_party_dir)/indri/contrib/lemur',
    'antlr_root': '<(third_party_dir)/indri/contrib/antlr',
  },
  'targets': [
    {
      'target_name': 'server_main',
      'type': 'executable',
      'sources': [
        '<(server_root)/server_main.cpp',
      ],
      'include_dirs': [
        '<(indri_root)/include',
        '<(indri_root)/include/indri',
        '<(lemur_root)/include',
        '<(lemur_root)/include/lemur',
        '<(antlr_root)/include',
        '<(antlr_root)/include/antlr',
      ],
      'dependencies': [
        'gflags.gyp:gflags',
        'libjsonrpc.gyp:libjsonrpc',
        'indri.gyp:libindri',
        'indri.gyp:liblemur',
        'server_RunQuery',
        'server_dumpindex',
        'server_buildindex',
      ],
      'link_settings': {
        'libraries': [
          '-stdlib=libstdc++',
          '-lz',
          # '-lpthread',
          # '-ldl',
        ],
      },
    },
    {
      'target_name': 'server_RunQuery',
      'type': 'static_library',
      'sources': [
        '<(server_root)/RunQuery.cpp',
      ],
      'include_dirs': [
        '<(indri_root)/include',
        '<(indri_root)/include/indri',
        '<(lemur_root)/include',
        '<(lemur_root)/include/lemur',
        '<(antlr_root)/include',
        '<(antlr_root)/include/antlr',
      ],
      'dependencies': [
        'indri.gyp:libindri',
        'libjsonrpc.gyp:libjsonrpc',
      ],
      'direct_dependent_settings': {
        'include_dirs': [
          '<(server_root)',
        ],
      },
    },
    {
      'target_name': 'server_dumpindex',
      'type': 'static_library',
      'sources': [
        '<(server_root)/dumpindex.cpp',
      ],
      'include_dirs': [
        '<(indri_root)/include',
        '<(indri_root)/include/indri',
        '<(lemur_root)/include',
        '<(lemur_root)/include/lemur',
        '<(antlr_root)/include',
        '<(antlr_root)/include/antlr',
      ],
      'dependencies': [
        'indri.gyp:libindri',
        'libjsonrpc.gyp:libjsonrpc',
      ],
      'direct_dependent_settings': {
        'include_dirs': [
          '<(server_root)',
        ],
      },
    },
    {
      'target_name': 'server_buildindex',
      'type': 'static_library',
      'sources': [
        '<(server_root)/BuildIndex.cpp',
      ],
      'include_dirs': [
        '<(indri_root)/include',
        '<(indri_root)/include/indri',
        '<(lemur_root)/include',
        '<(lemur_root)/include/lemur',
        '<(antlr_root)/include',
        '<(antlr_root)/include/antlr',
      ],
      'dependencies': [
        'indri.gyp:libindri',
        'libjsonrpc.gyp:libjsonrpc',
      ],
      'direct_dependent_settings': {
        'include_dirs': [
          '<(server_root)',
        ],
      },
    },
  ],
}
