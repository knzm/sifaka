{
  'variables': {
    # Top directory of third party libraries.
    'third_party_dir': '<(DEPTH)/third_party',

    'clang%': 0,
    'warning_cflags%': [],
    'warning_cflags_c%': [],
    'warning_cflags_cc%': [],
    'warning_ldflags%': [],

    'target_arch%': '',

    'conditions': [
      ['OS=="mac"', {
        'clang': 1,
      }],
    ],

    'gcc_cflags': [
      '-funwind-tables',
    ],

    'gcc_warning_cflags': [
      # '-Werror',
      '-Wall',
      # less harmful
      '-Wno-unused-function',
      '-Wno-unused-value',
      '-Wno-unused-variable',
      '-Wno-deprecated-declarations',
      # matter of coding style
      '-Wno-missing-braces',
      # bad types
      '-Wno-sign-compare',
      '-Wno-write-strings',
    ],
    'clang_warning_cflags': [
      # '-Werror',
      '-Wall',
      # less harmful
      '-Wno-reorder',
      '-Wno-unused-function',
      '-Wno-unused-private-field',
      '-Wno-unused-value',
      '-Wno-unused-variable',
      '-Wno-deprecated-declarations',
      '-Wno-deprecated-writable-strings',
      # matter of coding style
      '-Wno-missing-braces',
      '-Wno-shift-op-parentheses',
      '-Wno-sometimes-uninitialized',
      # bad types
      '-Wno-null-conversion',
      '-Wno-pointer-sign',
      '-Wno-incompatible-pointer-types-discards-qualifiers',
      # XXX possibly danger
      '-Wno-delete-non-virtual-dtor',
      '-Wno-dynamic-class-memaccess',
      '-Wno-overloaded-virtual',
      # candidates
      # '-Wno-char-subscripts',
      # '-Wno-sign-compare',
      # '-Wwrite-strings',
    ],
  },
  'target_defaults': {
    'variables': {
      'release_extra_cflags%': ['-O2'],
      'debug_extra_cflags%': ['-O0', '-g'],
      'mac_release_optimization%': '2',  # Use -O2 unless overridden
      'mac_debug_optimization%': '0',    # Use -O0 unless overridden
      'other_cflags%': [],
      'conditions': [
        ['clang==0', {
          'other_cflags%': ['<@(gcc_cflags)'],
        }],
      ],
    },
    'configurations': {
      'Common_Base': {
        'abstract': 1,
        'conditions': [
          ['OS=="mac"', {
            'defines': [
              'P_NEEDS_GNU_CXX_NAMESPACE=1',
            ],
            # 'cflags_cc': ['-stdlib=libstdc++'],
            'ldflags': ['-stdlib=libstdc++'],
            'xcode_settings': {
              'CLANG_CXX_LIBRARY': 'libstdc++',
              # 'OTHER_LDFLAGS': ['-stdlib=libstdc++'],
            },
          }],
        ],
      },
      'Debug': {
        'inherit_from': ['Common_Base'],
        'defines': [
          'DEBUG',
        ],
        'cflags': [
          '<@(other_cflags)',
          '<@(debug_extra_cflags)',
          '<@(warning_cflags)',
        ],
        'cflags_c': [
          '<@(warning_cflags_c)',
        ],
        'cflags_cc': [
          '<@(warning_cflags_cc)',
        ],
        'ldflags': [
          '<@(warning_ldflags)',
        ],
        'xcode_settings': {
          'COPY_PHASE_STRIP': 'NO',
          'GCC_OPTIMIZATION_LEVEL': '<(mac_debug_optimization)',
          'GCC_INLINES_ARE_PRIVATE_EXTERN': 'YES',
          'OTHER_CFLAGS': [ '<@(other_cflags)', '<@(debug_extra_cflags)', ],
          'WARNING_CFLAGS': ['<@(clang_warning_cflags)'],
        },
      },
      'Release': {
        'inherit_from': ['Common_Base'],
        'defines': [
          'NDEBUG',
        ],
        'cflags': [
          '<@(other_cflags)',
          '<@(release_extra_cflags)',
          '<@(warning_cflags)',
        ],
        'cflags_c': [
          '<@(warning_cflags_c)',
        ],
        'cflags_cc': [
          '<@(warning_cflags_cc)',
        ],
        'ldflags': [
          '<@(warning_ldflags)',
        ],
        'xcode_settings': {
          'DEAD_CODE_STRIPPING': 'YES',  # -Wl,-dead_strip
          'GCC_OPTIMIZATION_LEVEL': '<(mac_release_optimization)',
          'OTHER_CFLAGS': [ '<@(other_cflags)', '<@(release_extra_cflags)', ],
          'WARNING_CFLAGS': ['<@(clang_warning_cflags)'],
        },
      },
    },
    'default_configuration': 'Debug',

    'conditions': [
      ['target_arch=="ia32"', {
        'cflags': [
          '-msse2',
          '-mfpmath=sse',
          '-mmmx',  # Allows mmintrin.h for MMX intrinsics.
          '-m32',
        ],
        'ldflags': [
          '-m32',
        ],
      }],
      ['target_arch=="x64"', {
        'cflags': [
          '-m64',
          '-march=x86-64',
        ],
        'ldflags': [
          '-m64',
        ],
      }],

      ['clang==1', {
        'variables': {
          'warning_cflags': ['<@(clang_warning_cflags)'],
        }
      }],
      ['clang==0', {
        'variables': {
          'warning_cflags': ['<@(gcc_warning_cflags)'],
          'warning_cflags_c': ['-Wno-pointer-sign'],
          'warning_cflags_cc': ['-Wno-reorder'],
          'warning_ldflags': ['-Wno-deprecated-declarations'],
        }
      }],

      [ 'OS=="freebsd" or (OS=="linux" and target_arch!="ia32")', {
        'cflags': [ '-fPIC' ],
      }]
    ],
  },
  'xcode_settings': {
    'conditions': [
      ['target_arch=="x64"', {
        'ARCHS': [
          'x86_64'
        ],
      }],
      ['target_arch=="ia32"', {
        'ARCHS': [
          'i386'
        ],
      }],
    ],
  },
}

