#ifndef SIFAKA_SERVER_BUILDINDEX
#define SIFAKA_SERVER_BUILDINDEX

#include <string>
#include "indri/Parameters.hpp"

typedef std::vector< std::pair< std::string, std::string > > metadata_t;
typedef std::pair< std::string, metadata_t > document_t;

void build_environment(indri::api::IndexEnvironment& env,
                       /* const */ indri::api::Parameters& parameters,
                       const std::string fileClass = "");

bool BuildIndexMain(/* const */ indri::api::Parameters& parameters,
                    const std::string& fileClass,
                    const std::vector< document_t >& documents);

#endif // SIFAKA_SERVER_BUILDINDEX
