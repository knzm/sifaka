#ifndef SIFAKA_SERVER_RUNQUERY
#define SIFAKA_SERVER_RUNQUERY

#include "indri/Parameters.hpp"

void IndriRunQueryMain(Json::Value& response, indri::api::Parameters& param);

#endif // SIFAKA_SERVER_RUNQUERY
