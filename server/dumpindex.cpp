/*==========================================================================
 * Copyright (c) 2004 University of Massachusetts.  All Rights Reserved.
 *
 * Use of the Lemur Toolkit for Language Modeling and Information Retrieval
 * is subject to the terms of the software license set forth in the LICENSE
 * file included with this software, and also available at
 * http://www.lemurproject.org/license.html
 *
 *==========================================================================
*/

//
// dumpindex
//
// 13 September 2004 -- tds
//

#include <iostream>
#include <jsonrpc/rpc.h>

#include "indri/Repository.hpp"
#include "indri/CompressedCollection.hpp"
#include "indri/LocalQueryServer.hpp"
#include "indri/ScopedLock.hpp"
#include "indri/QueryEnvironment.hpp"
#include "lemur/Exception.hpp"
#include "antlr/ANTLRException.hpp"

#include "dumpindex.h"

using namespace jsonrpc;


void get_document_expression_count( const std::string& indexName, const std::string& expression, Json::Value& out ) {
  indri::api::QueryEnvironment env;
  // compute the expression list using the QueryEnvironment API
  env.addIndex( indexName );
  double result = env.documentExpressionCount( expression );
  env.close();
  out["count"] = result;
}

void get_expression_count( const std::string& indexName, const std::string& expression, Json::Value& out ) {
  indri::api::QueryEnvironment env;

  // compute the expression list using the QueryEnvironment API
  env.addIndex( indexName );
  double result = env.expressionCount( expression );
  env.close();

  out["count"] = result;
}

void get_expression_list( const std::string& indexName, const std::string& expression, Json::Value& out ) {
  indri::api::QueryEnvironment env;

  // compute the expression list using the QueryEnvironment API
  env.addIndex( indexName );
  std::vector<indri::api::ScoredExtentResult> result = env.expressionList( expression );


  out["termCount"] = (UINT32)env.termCount();
  out["documentCount"] = (UINT32)env.documentCount();

  env.close();

  out["expressions"] = Json::Value();

  // now, return the results in the format:
  // documentID weight begin end
  for( size_t i=0; i<result.size(); i++ ) {
    Json::Value item;
    item["document"] = result[i].document;
    item["score"] = result[i].score;
    item["begin"] = result[i].begin;
    item["end"] = result[i].end;

    out["expressions"].append(item);
  }
}

//
// Get the whole inverted file.  Each term entry starts with
// a term statistics header (term, termCount, documentCount)
// followed by indented rows (one per document) of the form:
// (document, numPositions, pos1, pos2, ... posN ).
//

void get_invfile( indri::collection::Repository& r, Json::Value& out ) {
  indri::collection::Repository::index_state state = r.indexes();

  indri::index::Index* index = (*state)[0];
  indri::index::DocListFileIterator* iter = index->docListFileIterator();
  iter->startIteration();

  out["termCount"] = (UINT32)index->termCount();
  out["documentCount"] = (UINT32)index->documentCount();

  out["terms"] = Json::Value();

  while( !iter->finished() ) {
    indri::index::DocListFileIterator::DocListData* entry = iter->currentEntry();
    indri::index::TermData* termData = entry->termData;

    Json::Value term;

    term["term"] = termData->term;
    term["totalCount"] = (UINT32) termData->corpus.totalCount;
    term["documentCount"] = (UINT32) termData->corpus.documentCount;

    term["documents"] = Json::Value();

    entry->iterator->startIteration();

    while( !entry->iterator->finished() ) {
      indri::index::DocListIterator::DocumentData* doc = entry->iterator->currentEntry();

      Json::Value item;

      item["document"] = (UINT32) doc->document;
      item["size"] = (UINT32) doc->positions.size();

      item["positions"] = Json::Value();

      for( size_t i=0; i<doc->positions.size(); i++ ) {
        item["positions"].append(doc->positions[i]);
      }

      entry->iterator->nextEntry();

      term["documents"].append(item);
    }

    iter->nextEntry();

    out["terms"].append(term);
  }

  delete iter;
}

//
// Returns the vocabulary of the index, including term statistics.
//

void get_vocabulary( indri::collection::Repository& r, std::string field, Json::Value& out ) {
  indri::collection::Repository::index_state state = r.indexes();

  indri::index::Index* index = (*state)[0];

  int fieldId = -1;
  if (! field.empty()) {
    fieldId = index->field(field);
    if (fieldId == 0) {
      out["termCount"] = 0;
      out["documentCount"] = 0;
      out["terms"] = Json::Value();
      return;
    }
  }

  if (fieldId > 0) {
    out["termCount"] = (UINT32) index->fieldTermCount(field);
    out["documentCount"] = (UINT32) index->fieldDocumentCount(field);
  }
  else {
    out["termCount"] = (UINT32) index->termCount();
    out["documentCount"] = (UINT32) index->documentCount();
  }

  std::vector< Json::Value* > items;

  out["terms"] = Json::Value();

  indri::index::VocabularyIterator* iter = index->vocabularyIterator();
  iter->startIteration();
  while( !iter->finished() ) {
    indri::index::DiskTermData* entry = iter->currentEntry();
    indri::index::TermData* termData = entry->termData;

    UINT64 totalCount;
    UINT64 documentCount;

    if (fieldId > 0) {
      totalCount = termData->fields[fieldId-1].totalCount;
      documentCount = termData->fields[fieldId-1].documentCount;
    }
    else {
      totalCount = termData->corpus.totalCount;
      documentCount = termData->corpus.documentCount;
    }

    if (documentCount > 0) {
      Json::Value item;

      item["term"] = termData->term;
      item["totalCount"] = (UINT32) totalCount;
      item["documentCount"] = (UINT32) documentCount;

      out["terms"].append(item);
    }

    iter->nextEntry();
  }

  delete iter;
}

void get_field_positions( indri::collection::Repository& r, const std::string& fieldString, Json::Value& out ) {
  indri::server::LocalQueryServer local(r);

  UINT64 totalCount = local.termCount();

  out["totalCount"] = (UINT32)totalCount;

  out["documents"] = Json::Value();

  indri::collection::Repository::index_state state = r.indexes();

  for( size_t i=0; i<state->size(); i++ ) {
    indri::index::Index* index = (*state)[i];
    indri::thread::ScopedLock( index->iteratorLock() );

    indri::index::DocExtentListIterator* iter = index->fieldListIterator( fieldString );
    if (iter == NULL) continue;

    iter->startIteration();

    indri::index::DocExtentListIterator::DocumentExtentData* entry;

    for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
      entry = iter->currentEntry();

      lemur::api::DOCID_T documentID = entry->document;

      Json::Value document;

      document["id"] = (UINT32) documentID;
      document["length"] = (UINT32) index->documentLength( documentID );

      document["positions"] = Json::Value();

      /*
      std::vector<lemur::api::DOCID_T> documentIDs;
      documentIDs.push_back(documentID);

      indri::server::QueryServerVectorsResponse* response = local.documentVectors( documentIDs );
      indri::api::DocumentVector* docVector = response->getResults()[0];
      */

      size_t count = entry->extents.size();

      for( size_t i=0; i<count; i++ ) {
        int begin = entry->extents[i].begin;
        int end = entry->extents[i].end;

        Json::Value pos;

        pos["begin"] = (UINT32)begin;
        pos["end"] = (UINT32)end;

        /*
        pos["terms"] = Json::Value();
        for (int position = begin; position < end; position++) {
          const std::string& stem = docVector->stems()[position];
          pos["terms"].append(stem);
        }
        */

        if( entry->numbers.size() ) {
          UINT64 number = entry->numbers[i];
          if ((number >> 32) == 0) {
            pos["number"] = (UINT32)number;
          }
          else {
            std::ostringstream ss;
            ss << number;
            pos["number"] = ss.str();
          }
        }

        document["positions"].append(pos);
      }

      out["documents"].append(document);
    }

    delete iter;
  }
}

void get_term_positions( indri::collection::Repository& r, const std::string& termString, Json::Value& out ) {
  std::string stem = r.processTerm( termString );
  indri::server::LocalQueryServer local(r);

  UINT64 totalCount = local.termCount();
  UINT64 termCount = local.termCount( termString );

  out["stem"] = stem;
  out["termCount"] = (UINT32)termCount;
  out["totalCount"] = (UINT32)totalCount;

  out["documents"] = Json::Value();

  indri::collection::Repository::index_state state = r.indexes();

  for( size_t i=0; i<state->size(); i++ ) {
    indri::index::Index* index = (*state)[i];
    indri::thread::ScopedLock( index->iteratorLock() );

    indri::index::DocListIterator* iter = index->docListIterator( stem );
    if (iter == NULL) continue;

    iter->startIteration();

    int doc = 0;
    indri::index::DocListIterator::DocumentData* entry;

    for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
      entry = (indri::index::DocListIterator::DocumentData*) iter->currentEntry();

      Json::Value document;

      document["document"] = entry->document;
      document["size"] = (UINT32)entry->positions.size();
      document["length"] = (UINT32)index->documentLength( entry->document );

      document["positions"] = Json::Value();

      size_t count = entry->positions.size();

      for( size_t i=0; i<count; i++ ) {
        document["positions"].append(entry->positions[i]);
      }

      out["documents"].append(document);
    }

    delete iter;
  }
}

void get_term_counts( indri::collection::Repository& r, const std::string& termString, Json::Value& out ) {
  std::string stem = r.processTerm( termString );
  indri::server::LocalQueryServer local(r);

  UINT64 totalCount = local.termCount();
  UINT64 termCount = local.termCount( termString );

  out["stem"] = stem;
  out["termCount"] = (UINT32)termCount;
  out["totalCount"] = (UINT32)totalCount;

  out["documents"] = Json::Value();

  indri::collection::Repository::index_state state = r.indexes();

  for( size_t i=0; i<state->size(); i++ ) {
    indri::index::Index* index = (*state)[i];
    indri::thread::ScopedLock( index->iteratorLock() );

    indri::index::DocListIterator* iter = index->docListIterator( stem );
    if (iter == NULL) continue;

    iter->startIteration();

    // int doc = 0;

    for( iter->startIteration(); iter->finished() == false; iter->nextEntry() ) {
      indri::index::DocListIterator::DocumentData* entry;

      entry = iter->currentEntry();

      Json::Value document;

      document["document"] = entry->document;
      document["size"] = (UINT32)entry->positions.size();
      document["length"] = (UINT32)index->documentLength( entry->document );

      out["documents"].append(document);
    }

    delete iter;
  }
}

void get_document_name( indri::collection::Repository& r, const std::string number, Json::Value& out ) {
  indri::collection::CompressedCollection* collection = r.collection();
  //  std::string documentName = collection->retrieveMetadatum( atoi( number.c_str() ), "docid" );
  std::string documentName = collection->retrieveMetadatum( atoi( number.c_str() ), "docno" );
  out["docno"] = documentName;
}

void get_document_text( indri::collection::Repository& r, const std::string number, Json::Value& out ) {
  int documentID = atoi( number.c_str() );
  indri::collection::CompressedCollection* collection = r.collection();
  indri::api::ParsedDocument* document = collection->retrieve( documentID );

  out["documentText"] = document->text;

  delete document;
}

void get_document_data( indri::collection::Repository& r, const std::string number, Json::Value& out ) {
  int documentID = atoi( number.c_str() );
  indri::collection::CompressedCollection* collection = r.collection();
  indri::api::ParsedDocument* document = collection->retrieve( documentID );

  // Metadata

  Json::Value meta;

  for( size_t i=0; i<document->metadata.size(); i++ ) {
    if( document->metadata[i].key[0] == '#' )
      continue;

    const char* key = document->metadata[i].key;
    const void* value = document->metadata[i].value;

    meta[key] = value;
  }

  out["meta"] = meta;

  // Positions

  Json::Value positions;

  for( size_t i=0; i<document->positions.size(); i++ ) {
    Json::Value pos;

    pos["begin"] = document->positions[i].begin;
    pos["end"] = document->positions[i].end;

    positions.append(pos);
  }

  out["positions"] = positions;

  // Tags

  Json::Value tags;

  for( size_t i=0; i<document->tags.size(); i++ ) {
    Json::Value tag;

    tag["name"] = document->tags[i]->name;
    tag["begin"] = document->tags[i]->begin;
    tag["end"] = document->tags[i]->end;
    tag["number"] = (UINT32)document->tags[i]->number;
  }

  out["tags"] = tags;

  // Text

  out["text"] = document->text;

  // Content

  out["content"] = document->getContent();

  delete document;
}

void get_document_vector( indri::collection::Repository& r, const std::string number, Json::Value& out ) {
  indri::server::LocalQueryServer local(r);
  lemur::api::DOCID_T documentID = atoi( number.c_str() );

  std::vector<lemur::api::DOCID_T> documentIDs;
  documentIDs.push_back(documentID);

  indri::server::QueryServerVectorsResponse* response = local.documentVectors( documentIDs );

  out["fields"] = Json::Value();
  out["terms"] = Json::Value();

  if( response->getResults().size() ) {
    indri::api::DocumentVector* docVector = response->getResults()[0];

    // Fields

    for( size_t i=0; i<docVector->fields().size(); i++ ) {
      const indri::api::DocumentVector::Field& field = docVector->fields()[i];

      Json::Value item;

      item["name"] = field.name;
      item["begin"] = field.begin;
      item["end"] = field.end;
      item["number"] = (UINT32)field.number;

      out["fields"].append(item);
    }

    // Terms

    for( size_t i=0; i<docVector->positions().size(); i++ ) {
      int position = docVector->positions()[i];
      const std::string& stem = docVector->stems()[position];

      Json::Value item;

      item["position"] = position;
      item["stem"] = stem;

      out["terms"].append(item);
    }

    delete docVector;
  }

  delete response;
}

void get_document_id( indri::collection::Repository& r, const std::string attributeName, const std::string attributeValue, Json::Value& out ) {
  indri::collection::CompressedCollection* collection = r.collection();
  std::vector<lemur::api::DOCID_T> documentIDs;

  documentIDs = collection->retrieveIDByMetadatum( attributeName, attributeValue );

  Json::Value documents;

  for( size_t i=0; i<documentIDs.size(); i++ ) {
    documents.append(documentIDs[i]);
  }

  out["documents"] = documents;
}

void get_repository_stats( indri::collection::Repository& r, Json::Value& out ) {
  indri::server::LocalQueryServer local(r);
  UINT64 termCount = local.termCount();
  UINT64 docCount = local.documentCount();
  std::vector<std::string> fields = local.fieldList();
  indri::collection::Repository::index_state state = r.indexes();
  UINT64 uniqueTermCount = 0;
  UINT64 fieldDocumentCount[fields.size()];
  UINT64 fieldTermCount[fields.size()];
  for( size_t i=0; i<fields.size(); i++ ) {
    fieldDocumentCount[i] = 0;
    fieldTermCount[i] = 0;
  }
  for( size_t i=0; i<state->size(); i++ ) {
    indri::index::Index* index = (*state)[i];
    uniqueTermCount += index->uniqueTermCount();
    for( size_t i=0; i<fields.size(); i++ ) {
      fieldDocumentCount[i] += index->fieldDocumentCount(fields[i]);
      fieldTermCount[i] += index->fieldTermCount(fields[i]);
    }
  }

  out["docCount"] = (UINT32)docCount;
  out["uniqueTermCount"] = (UINT32)uniqueTermCount;
  out["termCount"] = (UINT32)termCount;

  out["fields"] = Json::Value();

  for( size_t i=0; i<fields.size(); i++ ) {
    Json::Value item;
    item["name"] = fields[i];
    item["docCount"] = (UINT32)fieldDocumentCount[i];
    item["termCount"] = (UINT32)fieldTermCount[i];

    out["fields"].append(item);
  }
}

void delete_document( indri::collection::Repository& r, const std::string stringDocumentID ) {
  lemur::api::DOCID_T documentID = (lemur::api::DOCID_T) string_to_i64( stringDocumentID.c_str() );
  r.deleteDocument( documentID );
}

void error(Json::Value& out) {
  out["error"] = true;
  out["message"] = "Invalid arguments";
}

#define REQUIRE_ARGS(n) { if( args.size() < n ) { error(result); return result; } }

Json::Value dumpindex_main(const std::string repName, const std::string command, const std::vector< std::string > args) {
  indri::collection::Repository r;
  Json::Value result;

  try {
    if ( command == "c" || command == "compact" ) {
      r.open( repName );
      r.compact();
      r.close();
    }
    else if ( command == "del" || command == "delete" ) {
      REQUIRE_ARGS(1);
      std::string docId = args[0];
      r.open( repName );
      delete_document( r, docId );
      r.close();
    }
    else if ( command == "t" || command == "term" ) {
      REQUIRE_ARGS(1);
      std::string term = args[0];
      r.openRead( repName );
      get_term_counts( r, term, result );
      r.close();
    }
    else if ( command == "tp" || command == "termpositions" ) {
      REQUIRE_ARGS(1);
      std::string term = args[0];
      r.openRead( repName );
      get_term_positions( r, term, result );
      r.close();
    }
    else if ( command == "fp" || command == "fieldpositions" ) {
      REQUIRE_ARGS(1);
      std::string field = args[0];
      r.openRead( repName );
      get_field_positions( r, field, result );
      r.close();
    }
    else if ( command == "e" || command == "expression" ) {
      REQUIRE_ARGS(1);
      std::string expression = args[0];
      r.openRead( repName );
      get_expression_list( repName, expression, result );
      r.close();
    }
    else if ( command == "x" || command == "xcount" ) {
      REQUIRE_ARGS(1);
      std::string expression = args[0];
      r.openRead( repName );
      get_expression_count( repName, expression, result );
      r.close();
    }
    else if ( command == "dx" || command == "dxcount" ) {
      REQUIRE_ARGS(1);
      std::string expression = args[0];
      r.openRead( repName );
      get_document_expression_count( repName, expression, result );
      r.close();
    }
    else if ( command == "dn" || command == "documentname" ) {
      REQUIRE_ARGS(1);
      std::string docId = args[0];
      r.openRead( repName );
      get_document_name( r, docId, result );
      r.close();
    }
    else if ( command == "dt" || command == "documenttext" ) {
      REQUIRE_ARGS(1);
      std::string docId = args[0];
      r.openRead( repName );
      get_document_text( r, docId, result );
      r.close();
    }
    else if ( command == "dd" || command == "documentdata" ) {
      REQUIRE_ARGS(1);
      std::string docId = args[0];
      r.openRead( repName );
      get_document_data( r, docId, result );
      r.close();
    }
    else if ( command == "dv" || command == "documentvector" ) {
      REQUIRE_ARGS(1);
      std::string docId = args[0];
      r.openRead( repName );
      get_document_vector( r, docId, result );
      r.close();
    }
    else if ( command == "di" || command == "documentid" ) {
      REQUIRE_ARGS(2);
      std::string field = args[0];
      std::string value = args[1];
      r.openRead( repName );
      get_document_id( r, field, value, result );
      r.close();
    }
    else if ( command == "il" || command == "invlist" ) {
      r.openRead( repName );
      get_invfile( r, result );
      r.close();
    }
    else if ( command == "v" || command == "vocabulary" ) {
      r.openRead( repName );
      get_vocabulary( r, "", result );
      r.close();
    }
    else if ( command == "fv" || command == "fieldvocabulary" ) {
      REQUIRE_ARGS(1);
      std::string field = args[0];
      r.openRead( repName );
      get_vocabulary( r, field, result );
      r.close();
    }
    else if ( command == "s" || command == "stats" ) {
      r.openRead( repName );
      get_repository_stats( r, result );
      r.close();
    }
    else {
      error(result);
    }
  }
  catch ( lemur::api::Exception& e ) {
    e.writeMessage();
    result["error"] = true;
    result["message"] = e.what();
  }
  catch ( antlr::ANTLRException& e) {
    std::cerr << e.toString() << std::endl;
    result["error"] = true;
    result["message"] = e.toString();
  }

  if (! result.isMember("error")) {
    result["error"] = false;
  }

  return result;
}
