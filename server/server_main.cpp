#include <stdio.h>
#include <string>
#include <iostream>
#include <signal.h>
#include <ctime>
#include <jsonrpc/rpc.h>
#include <jsonrpc/connectors/httpserver.h>
#include <gflags/gflags.h>

#include "indri/Parameters.hpp"
#include "indri/Repository.hpp"
#include "indri/IndexEnvironment.hpp"
#include "indri/indri-platform.h"
#include "lemur/Exception.hpp"

#include "RunQuery.h"
#include "dumpindex.h"
#include "BuildIndex.h"

DEFINE_int32(threads, 10, "Number of threads");
DEFINE_bool(dump_request, false, "Dump each request");

#define ASSERT_JSON_STRING(o) \
  if (! (o).isString()) { \
    std::cerr << __FILE__ << "(" << __LINE__ << "): " << #o << " is not a string" << std::endl; \
    throw JsonRpcException(Errors::ERROR_RPC_INTERNAL_ERROR); \
  }

using namespace jsonrpc;

class IndriServer : public AbstractServer<IndriServer> {
    typedef AbstractServer<IndriServer> super;
    indri::api::Parameters m_parameters;
public:
    IndriServer(AbstractServerConnector *connector, indri::api::Parameters& parameters)
        : AbstractServer<IndriServer>(connector), m_parameters(parameters)
    {
        Procedure *proc;

        // status() -> string
        proc = new Procedure("status", PARAMS_BY_NAME, JSON_STRING, NULL);
        this->bindAndAddMethod(proc, &IndriServer::status);

        // run_query() -> array
        proc = new Procedure("run_query", PARAMS_BY_NAME, JSON_ARRAY, NULL);
        this->bindAndAddMethod(proc, &IndriServer::run_query);

        // dumpindex() -> object
        proc = new Procedure("dumpindex", PARAMS_BY_NAME, JSON_OBJECT, NULL);
        this->bindAndAddMethod(proc, &IndriServer::dumpindex);

        // buildindex() -> boolean
        proc = new Procedure("buildindex", PARAMS_BY_NAME, JSON_BOOLEAN, NULL);
        this->bindAndAddMethod(proc, &IndriServer::buildindex);
    }

    std::string repositoryPath() /* const */
    {
        return m_parameters["index"];
    }

    virtual void handleMethodCall(Procedure* proc, const Json::Value& input, Json::Value& output)
    {
        std::string name = proc->GetProcedureName();

        char date[64];
        time_t t = ::time(NULL);
        struct tm tm;
        ::localtime_r(&t, &tm);
        strftime(date, sizeof(date), "%d/%b/%Y:%H:%M:%S %z", &tm);

        std::cout << "[" << date << "] "
                  << "Request " << name;
        if (FLAGS_dump_request) {
            std::cout << input;
        }
        std::cout << std::endl;

        try {
            super::handleMethodCall(proc, input, output);
        }
        catch (lemur::api::Exception& e) {
            e.writeMessage();
            throw JsonRpcException(Errors::ERROR_RPC_INTERNAL_ERROR);
        }
    }

    void status(const Json::Value& request, Json::Value& response)
    {
        response = "Ok";
    }

    void run_query(const Json::Value& request, Json::Value& response)
    {
        indri::api::Parameters param;

        // deep copy
        param = m_parameters;

        if (! param.exists("trecFormat")) {
            param.append("trecFormat").set("false");
        }

        const char* knownParams[] = {
            "query", "count", "queryOffset",
            "rule", "stopper", "maxWildcardTerms", "baseline",
            "fbDocs", "fbTerms", "fbMu", "fbOrigWeight",
            "printSnippets", "printDocuments",
            // "printQuery", "printPassages",
        };

        int n = sizeof(knownParams) / sizeof(knownParams[0]);
        for (int i = 0; i < n; i++) {
            const char* name = knownParams[i];
            if (request.isMember(name)) {
                const Json::Value value = request[name];
                ASSERT_JSON_STRING(value);
                param.remove(name);
                param.append(name).set(value.asString());
            }
        }

#if 0
        Json::Value::Members member(request.getMemberNames());

        for (Json::Value::Members::iterator it = member.begin();
             it != member.end();
             ++it) {
            const std::string& name = *it;
            const Json::Value& value = request[name];
            ASSERT_JSON_STRING(value);
            param.remove(name);
            param.append(name).set(value.asString());
        }
#endif

        IndriRunQueryMain(response, param);
    }

    void dumpindex(const Json::Value& request, Json::Value& response)
    {
        const Json::Value command_value = request["command"];
        const Json::Value args_value = request["args"];

        ASSERT_JSON_STRING(command_value);
        std::string command = command_value.asString();
        std::vector< std::string > args;
        for (Json::Value::const_iterator it = args_value.begin();
             it != args_value.end();
             ++it) {
            ASSERT_JSON_STRING(*it);
            args.push_back((*it).asString());
        }

        response = dumpindex_main(repositoryPath(), command, args);
    }

    void buildindex(const Json::Value& request, Json::Value& response)
    {
        const Json::Value fileClass_value = request["fileClass"];
        const Json::Value documents_value = request["documents"];

        ASSERT_JSON_STRING(fileClass_value);
        std::string fileClass = fileClass_value.asString();

        std::vector< document_t > documents;

        for (Json::Value::const_iterator doc_it = documents_value.begin();
             doc_it != documents_value.end();
             ++doc_it) {
            const Json::Value document_value = (*doc_it)["document"];
            ASSERT_JSON_STRING(document_value);
            const std::string documentString = document_value.asString();

            const Json::Value metadata_value = (*doc_it)["metadata"];

            metadata_t metadata;
            for (Json::Value::const_iterator meta_it = metadata_value.begin();
                 meta_it != metadata_value.end();
                 ++meta_it) {
                ASSERT_JSON_STRING(meta_it.key());
                const std::string& key = meta_it.key().asString();

                ASSERT_JSON_STRING(metadata_value[key]);
                const std::string& value = metadata_value[key].asString();

                std::pair< std::string, std::string > pair;
                pair.first = key;
                pair.second = value;

                metadata.push_back(pair);
            }

            documents.push_back(document_t(documentString, metadata));
        }

        bool ret = BuildIndexMain(m_parameters, fileClass, documents);
        response["result"] = ret;
    }
};

int main(int argc, char** argv) {
    google::SetUsageMessage("[options] params_path");
    google::ParseCommandLineFlags(&argc, &argv, true);

    if (argc < 2) {
        std::cout << "Missing arguments" << std::endl;
        return 0;
    }

    std::string params_path = argv[1];

    indri::api::Parameters& parameters = indri::api::Parameters::instance();
    try {
        parameters.loadFile(params_path);
    } catch(lemur::api::Exception& e) {
        LEMUR_ABORT(e);
    }

    int port = parameters.get("port", INDRID_PORT);
    std::string repositoryPath = parameters["index"];

    indri::collection::Repository repository;
    if (! repository.exists(repositoryPath)) {
        indri::api::IndexEnvironment env;
        build_environment(env, parameters);
        env.create(repositoryPath);
        env.close();
    }

    bool enableSpecification = true;
    std::string sslcert = "";
    int threads = FLAGS_threads;

    try {
        AbstractServerConnector *connector =
            new HttpServer(port, enableSpecification, sslcert, threads);
        IndriServer serv(connector, parameters);
        if (! serv.StartListening()) {
            std::cout << "Error starting Server" << std::endl;
            return 1;
        }

        int signo;
        sigset_t ss;

        sigemptyset(&ss);
        if (sigaddset(&ss, SIGINT) != 0) {
            std::cout << "Error initializing signal" << std::endl;
            return 1;
        }

        std::cout << "Running at localhost:" << port << std::endl;

        while (1) {
            if (sigwait(&ss, &signo) == 0) {
                // received SIGINT
                break;
            }
        }

        std::cout << "Shutting down Server..." << std::endl;
        serv.StopListening();
    }
    catch (jsonrpc::JsonRpcException& e) {
        std::cerr << e.what() << endl;
    }

    return 0;
}
