#ifndef SIFAKA_SERVER_DUMPINDEX
#define SIFAKA_SERVER_DUMPINDEX

#include <string>
#include <jsonrpc/rpc.h>

Json::Value dumpindex_main(const std::string repName, const std::string command, const std::vector< std::string > args);

#endif // SIFAKA_SERVER_DUMPINDEX
