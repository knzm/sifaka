#!/usr/bin/env python

"""Script building Sifaka.

Typical usage:

  % python build_sifaka.py gyp gyp/server.gyp
  % python build_sifaka.py build
"""

from __future__ import print_function

import argparse
import glob
import os
import shutil
import subprocess
import sys

sifaka_root = os.path.dirname(os.path.realpath(__file__))

try:
    import gyp
except ImportError:
    sys.path.insert(0, os.path.join(sifaka_root, 'third_party', 'gyp', 'pylib'))
    import gyp


def get_parser(description=None):
    parser = argparse.ArgumentParser(description=description)
    # add common options
    return parser


def parse_gyp_options(args):
    parser = get_parser()
    parser.add_argument(
        '--format', '-f', dest='format', default='make',
        help='specify the output format to generate. (e.g. make)')
    parser.add_argument(
        '--arch', '-a', dest='target_arch', default='',
        help='specify the target arch to build. (ia32 or x64)')
    parser.add_argument('targets', nargs='*')
    return parser.parse_args(args)


def parse_build_options(args):
    parser = get_parser()
    parser.add_argument(
        '--configuration', '-c', dest='configuration', default='Debug',
        help='specify the build configuration. (Debug or Release)')
    return parser.parse_args(args)


def show_help():
    print('Usage: build_sifaka.py COMMAND [ARGS]')
    print('Commands: ')
    print('  gyp          Generate project files.')
    print('  build        Build the specified target.')
    print('  clean        Clean all the build files and directories.')
    print('')
    print('See also the comment in the script for typical usage.')
    sys.exit(1)


def gyp_main(args):
    options = parse_gyp_options(args)

    gyp_file_specified = False
    for target in options.targets:
        if target.endswith('.gyp'):
            gyp_file_specified = True
            break

    targets = options.targets
    if not gyp_file_specified:
        targets += glob.glob('gyp/*.gyp')

    gyp_args = targets[:]
    gyp_args.append('--depth=%s' % os.path.relpath(sifaka_root))
    gyp_args.append('--format=%s' % options.format)
    if options.target_arch:
        gyp_args.append('-Dtarget_arch=%s' % options.target_arch)

    print('Updating projects from gyp files...')
    sys.stdout.flush()

    rc = gyp.main(gyp_args)
    if rc != 0:
        print('Error running GYP')
        sys.exit(rc)


def build_main(args):
    options = parse_build_options(args)

    make_command = os.getenv('BUILD_COMMAND', 'make')
    build_args = ['BUILDTYPE=%s' % options.configuration]

    subprocess.check_call([make_command] + build_args)


def clean_main(args):
    filenames = ["gyp-mac-tool", "Makefile"]
    filenames += glob.glob("gyp/*.Makefile")
    filenames += glob.glob("gyp/*.mk")
    filenames += glob.glob("gyp/*.xcodeproj")

    for filename in filenames:
        if os.path.exists(filename):
            os.remove(filename)

    dirnames = ["out"]

    for dirname in dirnames:
        if os.path.exists(dirname):
            shutil.rmtree(dirname)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        show_help()

    command = sys.argv[1]
    args = sys.argv[2:]

    command_map = {
        'gyp': gyp_main,
        'build': build_main,
        'clean': clean_main,
        }

    main = command_map.get(command)
    if main is None:
        show_help()

    main(args)
